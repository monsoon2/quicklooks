load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl" 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl" 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
;**********************************************************************
;-- Procedure:  add_Mollweide_labels                                 --;
;--    Add latitude labels to plot                                   --;
;**********************************************************************
undef("add_Mollweide_labels")
function add_Mollweide_labels(wks,map,latspc,lonspc)
local lat_values, lat_labels, nlat, txres, x_in, y_in, x_out, y_out
begin

  getvalues map
     "mpProjection"  :  mapproj
  end getvalues
    
  if (mapproj .ne. 15) then
      print("")
      print("Warning: add_Mollweide_labels function only available for Mollweide projection!")
      print("")
      return map
;  else
;      print("")
;      print("--> Add_Mollweide_labels:     add labels for Mollweide projection")
;      print("")
  end if
  
  minlat =  -90
  maxlat =   90

;-- pick some "nice" values for the latitude labels
  lat_values = ispan(minlat,maxlat,latspc) * 1.
  nlat       = dimsizes(lat_values)

;-- create the labels; add space before right labels, and after left labels
  lat_labels = where(lat_values.lt.0,abs(lat_values)+"~S~o~N~S",lat_values+"~S~o~N~N")
  lat_labels = where(lat_values.eq.0,"0~S~o~N~",lat_labels)
  lat_labels = where(abs(lat_values).eq.90,"",lat_labels) ;-- no label at lat=abs(90)

;---------------------
;-- latitude labels
;---------------------
  txres                  =  True             ;-- set text resources
  txres@txFontHeightF    =  0.010
  txres@txFontThicknessF =  2

  y_in  = lat_values                         ;-- plot coordinates
  x_in  = new(dimsizes(y_in),float)          ;-- plot coordinates
  x_out = new(dimsizes(x_in),float)          ;-- for NDC coordinates
  y_out = new(dimsizes(y_in),float)          ;-- for NDC coordinates
  
;-- left latitude labels
  txres@txJust = "CenterRight"
  x_in = -180.0
  datatondc(map,x_in,y_in,x_out,y_out)
  gsn_text_ndc(wks, lat_labels, x_out-0.015, y_out, txres)

;-- right latitude labels
  txres@txJust = "CenterLeft"
  x_in = 180.0
  datatondc(map,x_in,y_in,x_out,y_out)
  gsn_text_ndc(wks, lat_labels, x_out+0.015, y_out, txres)

  return(map)
  
end   ;-- end function add_Mollweide_labels


undef("define_draw_strings")
 procedure define_draw_strings (Code,cArea,comment,wks,fldstd)
 
;************************************************
;  Define min max mean
;  Draw text on plot using NDC coordinates.
;************************************************
local txres

begin

;***compute mean


   meanV=dim_avg(Code*cArea)/dim_avg(cArea)

  minV=min(Code)
  maxV=max(Code)

  if (fldstd .gt. 0.) then
   statV="min="+sprintf("%8.3f", minV)+"   mean="+sprintf("%8.3f", meanV)+"   std="+sprintf("%8.3f", fldstd)+"  max="+sprintf("%8.3f", maxV)
  else 
   statV="min="+sprintf("%8.3f", minV)+"   mean="+sprintf("%8.3f", meanV)+"  max="+sprintf("%8.3f", maxV)
  end if

  pltdate = systemfunc("date +%d-%m-%Y")
;;;  print("actual_date: "+pltdate)

  txres               = True                    ; text mods desired
;  txres@txFontHeightF = 0.014                   ; font smaller. default big
;  gsn_text_ndc(wks,pltdate,0.85,0.15,txres)
;  gsn_text_ndc(wks,comment,0.18,0.15,txres)
  txres@txFontHeightF = 0.011 
print(statV)
  gsn_text_ndc(wks,statV,0.5,0.33,txres)
end
;***end procedure 

;---------------------------------------
;-- function attach_labelbar
;---------------------------------------
undef("attach_labelbar")
function attach_labelbar(wks,map,labels,colors)
local lbres, nlevels, amres
begin
  nlevels = dimsizes(labels)                     ; number of labels

  lbres                      =  True
  lbres@lbPerimOn            =  False            ; no label bar box
  lbres@lbOrientation        = "Horizontal"      ; orientation
  lbres@vpWidthF             =  0.7              ; width of labelbar
  lbres@vpHeightF            =  0.10             ; height of labelbar
  lbres@lbLabelFontHeightF   =  0.015            ; label font height
  lbres@lbLabelAlignment     = "InteriorEdges"   ; where to label
  lbres@lbMonoFillPattern    =  True             ; fill sold
  lbres@lbFillColors         =  colors           ; use colors
  lbid = gsn_create_labelbar (wks,nlevels+1,labels,lbres)

  amres                      =  True
  amres@amJust               = "TopCenter"       ; annotation alignment
  amres@amOrthogonalPosF     =  0.7              ; move annotation downward
;  amres@gsnMaximize          =  True             ; be sure to remaxmize output
  map@annoid                 =  gsn_add_annotation(map,lbid,amres)

  return(map)
end
;***end procedure attach_labelbar
undef("fast_cell")
 procedure fast_cell  (wks,var,x,y,cArea,vlon,vlat,cnres_in,missColor,pltName,\
                        mainTitle,subTitle,comment,fldstd )
;************************************************
;  
;************************************************
local plot, cnres, mpres, pres, res

begin

  nv   = dimsizes(vlon(0,:))                      ; no of points in polygon

  lonMin       = -180                             ; longitude minimum
  lonMax       =  180                             ; longitude maximum
  latMin       =  -90                             ; latitude minimum
  latMax       =   90                             ; latitude maximum
  mapCenter    =    0                             ; center of map

;  print("")
;  print("Data longitude min/max: " + min(vlon) + "   " + max(vlon)\
;      + "   cell points = " + nv)
;  print("")
;  print("Plot area: "+lonMin+","+lonMax+" "+latMin+","+latMax)
;  print("")

;***set contour resources
  res                      =  True
  res@gsnDraw              =  True        ;-- don't draw the plot
  res@gsnFrame             =  False        ;-- don't advance the frame
  res@cnLinesOn            =  False        ;-- don't draw contour lines
  res@cnInfoLabelOn        =  False        ;-- switch off contour info label
  res@cnFillOn             =  True         ;-- contour fill on

  res@cnFillMode           = "CellFill"    ;-- set fill mode
  res@sfXArray             =  x            ;-- transform x to mesh scalar field
  res@sfYArray             =  y            ;-- transform y to mesh scalar field
  res@sfXCellBounds        =  vlon         ;-- needed if cnFillMode= "CellFill"
  res@sfYCellBounds        =  vlat         ;-- needed if cnFillMod = "CellFill"
  
  res@lbLabelBarOn         =  True      ;-- don't draw a labelbar yet

  res@mpFillOn             =  True                    ;?-- fill map grey
  res@mpOutlineOn          =  True 
;  res@mpLandFillColor      =  18
  res@mpFillColors            =(/"transparent","transparent","transparent","transparent"/)
;***  res@mpMinLonF            =  lonMin         ;-- sub-region minimum longitude
;***  res@mpMaxLonF            =  lonMax         ;-- sub-region maximum longitude
;***  res@mpMinLatF            =  latMin         ;-- sub-region minimum latitude
;***  res@mpMaxLatF            =  latMax         ;-- sub-region maximum latitude

  res@mpGreatCircleLinesOn =  False          ;-- important: v6.2.0 False !!
  res@gsnCenterString      = " "
  res@gsnLeftString        = subTitle 
  res@gsnRightString       = mainTitle 
  res@txFontHeightF   = 0.017    

;;    res@gsnSpreadColors      = True    ; use full range of colors
  res@cnSpanFillPalette   =  False  ;instead of gsnSpreadColors, used by cnFillPalette
;res@lbAutoManage = False
;    res@pmLabelBarOrthogonalPosF = 0.1           ; move farther to plot
    res@lbOrientation        = "horizontal"      ; vertical label bar
    res@lbLabelFontHeightF   =  0.011            ; label font height

  res=cnres_in
;*** Mollweide
    res@mpProjection = "Mollweide"
    res@mpOutlineOn  = True
    res@mpPerimOn         = False             ; turn off perimeter
    res@mpGridAndLimbOn = True                    ; plot grid lines
    res@mpGridLatSpacingF = 30.
    res@mpGridLonSpacingF = 60.

;***create the contour plot, but don't draw it. 
;***We need it to get the colors of the data values

  plot = gsn_csm_contour_map(wks,var,res)

  print("min: "+min(var)+ "  max: "+ max(var))

;***define statistical Values and draw Values, comment, pltdate
    define_draw_strings (var,cArea,comment,wks,fldstd)

  frame(wks)                                ; now plot the frame

end 
;***end procedure fast_cell 
;************************************************
; main
;************************************************
begin

  comment = "no comment"


  values    = asciiread("var.txt",4,"string")
  run = values(0)
  meantime = values(1)
  yyyy = values(2)
  workdir = values(3)        ; plot & input directory


print("run: "+run)
print("meantime: "+meantime)
string_time=meantime+" "+yyyy
print("string_time: "+string_time)
suffix="_GL"

;***define plot
   pltType = "png"                        ; x11, eps, ps, pdf, png

  pltPath = workdir+"/"+run+"_"+meantime+"_"+yyyy+"_"
;************************************************
;  Read grid information
;  define the x-, y-values and the polygon points
;************************************************
;  GridInfoFileName = systemfunc("echo $GrdInfoFile")
;GridInfoFileName ="/work/ka1081/DYAMOND/ICON-80km-conv/icon_grid_0019_"+res_grid+"_G.nc"
;GridInfoFileName =workdir+"/icon_grid_"+res_grid+".nc"
GridInfoFileName =workdir+"/"+run+"_area.nc"  ;

print(GridInfoFileName)

  GridInfoFile= addfile( GridInfoFileName, "r" )

  rad2deg = 45./atan(1.)            ; radians to degrees

  x = GridInfoFile->clon *rad2deg   ; cell center, lon
  y = GridInfoFile->clat *rad2deg   ; cell center, lat
  cArea = GridInfoFile->cell_area   ; area of grid cell

; note: clon and clat are longitude and latitude of triangle centers.
;       Locations of the cell corners are given by 
;       clon_vertices and clat_vertices in the nc file.


  x!0     = "lon"
  x@units = "degrees_east"
  y!0     = "lat"
  y@units = "degrees_north"
  vlon    = GridInfoFile->clon_bnds * rad2deg
  vlat    = GridInfoFile->clat_bnds * rad2deg



fldstd=new(1,float)
fldstd=0.

;************************
;   latent heat flux 
;************************************************

;***setings
    Cvar="hfls"
    fili    = workdir+"/"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc"  
 print("fili "+fili)  
    mainTitle = "latent heat flux [W/m~S~2~N~,up]"
    subTitle  = run+" "+string_time

if (isfilepresent(fili)) then

;***read code 
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,:)         ; dims: (time, height, cell)
    var = var * (-1)
    print("ploted: "+mainTitle)

;***open plot 
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("BlueDarkRed18")
    colors = new((/20,4/),float)                  ;-- assign color array
    colors(0:17,:)  = cmap(0:17,:)          
    colors(18,:) = (/1.,1.,1.,1./)                ; add white
    colors(19,:) = (/.9,0.,.9,1./)                ; add pink
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = colors

    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/-60,-30,30,60,90,120,150,180,210,240/)
;    res@cnFillColors         = (/5,6,7,8,9,17,11,12,13,14,15,16/)
    res@cnFillColors         = (/1,7,18,10,11,12,13,15,16,17,19/)
    res@cnMissingValFillColor = missColor  

    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0.)

    delete(colors)
    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(res@cnFillPalette)
    delete(var)
    delete(cmap)
    delete(missColor)
end if

;************************************************
;   sensible heat flux 
;************************************************

;***setings
    Cvar="hfss"
    fili    = workdir+"/"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc" 
 print("fili "+fili)  
    mainTitle = "sensible heat flux [W/m~S~2~N~,up]"
    subTitle  = run+" "+string_time

if (isfilepresent(fili)) then

;***read code 
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,:)         ; dims: (time, height, cell)
    var = var * (-1)
    print("ploted: "+mainTitle)

;***open plot 
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("BlueDarkRed18")
    colors = new((/20,4/),float)                  ;-- assign color array
    colors(0:17,:)  = cmap(0:17,:)          
    colors(18,:) = (/1.,1.,1.,1./)                ; add white
    colors(19,:) = (/.9,0.,.9,1./)                ; add pink
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = colors

    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/-100,-80,-60,-40,-20,-10,10,20,40,60,80,100,120/)
;    res@cnFillColors         = (/5,6,7,8,9,10,17,11,12,13,14,15,16/)
    res@cnFillColors         = (/1,3,4,5,7,8,18,10,12,13,14,16,17,19/)
    res@cnMissingValFillColor = missColor  

    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0.)

    delete(colors)
    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(res@cnFillPalette)
    delete(var)
    delete(cmap)
    delete(missColor)
end if


;************************************************
;   olr
;************************************************

;***setings
    Cvar="olr"
    fili    = workdir+"/"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc" 
 print("fili "+fili)  
    mainTitle = "olr [W/m~S~2~N~]"
    subTitle  = run+" "+string_time

if (isfilepresent(fili)) then

;***read code 
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,:)         ; dims: (time, height, cell)
    var = (-1) *var
    print("ploted: "+mainTitle)

;***open plot 
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("MPL_Greys")
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = cmap
    res@cnSpanFillPalette   =  True  ;instead of gsnSpreadColors, used by cnFillPalette
    res@mpGeophysicalLineColor = "orange"
    res@cnLevelSelectionMode = "ManualLevels"
    res@cnMinLevelValF = 200
    res@cnMaxLevelValF = 320
    res@cnLevelSpacingF = 10

    res@cnMissingValFillColor = missColor

    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0.)

delete(res@mpGeophysicalLineColor) 
delete(res@cnSpanFillPalette)
    delete(res@cnFillPalette)
    delete(var)
    delete(cmap)
    delete(missColor)
end if

;************************************************
;   Wind speed
;************************************************

;***setings
    Cvar="sfcwind"
    fili    = workdir+"/"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc" 
 print("fili "+fili)  
    mainTitle = "Wind Speed [m/s]"
    subTitle  = run+" "+string_time

if (isfilepresent(fili)) then

;***read code 
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,0,:)         ; dims: (time, height, cell)
    print("ploted: "+mainTitle)

;***open plot sfcWind
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("wind_17lev")
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = cmap
    res@cnSpanFillPalette   =  True  ;instead of gsnSpreadColors, used by cnFillPalette
    res@cnLevelSelectionMode = "ManualLevels"
    res@cnMinLevelValF = 2
    res@cnMaxLevelValF = 20
    res@cnLevelSpacingF = 2

    res@cnMissingValFillColor = missColor

    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0.)

    delete(res@cnSpanFillPalette)
    delete(res@cnFillPalette)
    delete(var)
    delete(cmap)
    delete(missColor)
end if

;************************************************
;   Zonal wind stress CMOR: tauu only sea
;************************************************

;***setings
    Cvar="tauu"
    fili    = workdir+"/X"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc"  
 print("fili "+fili)  
    mainTitle = "Zonal wind stress [mN/m~S~2~N~]"
    subTitle  = run+" "+string_time



if (isfilepresent(fili)) then

;***read code tauu
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,:)       ;(time|0,cell|:)
    var = var * 1000  
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("amwg_blueyellowred")
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = cmap
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/-200,-100,-50,-20,0,20,50,100,200/)
    res@cnFillColors         = (/0,2,4,6,7,8,9,10,12,13,14/)
    res@cnMissingValFillColor = missColor

    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0.)

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(res@cnFillPalette)
    delete(var)
    delete(cmap)
    delete(missColor)
end if


;************************************************
;   Total cloud cover CMOR: clt
;************************************************

;***setings
    Cvar="clt"
    fili    = workdir+"/"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc" 
 print("fili "+fili)      
    mainTitle = "Total cloud cover [%]"
    subTitle  = run+" "+string_time


                
if (isfilepresent(fili)) then
;***read code clt
    File    = addfile (fili , "r")  
    var     = File->$Cvar$(0,:)       ;(time|0,cell|:) 
    var = var * 100    
;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix         ; plot name
    wks     = gsn_open_wks(pltType, pltName) 


    cmap =  read_colormap_file("amwg_blueyellowred")
    colors = new((/22,4/),float)                  ;-- assign color array
    colors(0:15,:)  = cmap(0:15,:)          
    colors(16,:) = (/1.,1.,1.,1./)                ; add white
    colors(17,:) = (/.8,.8,.8,1./)                ; add gray to color map
    colors(18,:) = (/.6,.6,.6,1./)
    colors(19,:) = (/.4,.4,.4,1./)
    colors(20,:) = (/.2,.2,.2,1./)
    colors(21,:) = (/.9,.9,.9,1./)                ; add grey

    missColor = (/0.5,0.5,0.5,0.5/) 
;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = colors
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/10,20,30,40,50,60,70,80,90/)
    res@cnFillColors         = (/1,3,4,6,7,21,17,18,19,20/)
;    res@cnFillColors         = (/1,4,6,7,17,18,19/)
    res@cnMissingValFillColor =  missColor    ; gray 

    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0. )

    delete(colors)
    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
    delete(res@cnFillPalette)
    delete(cmap)
    delete(missColor)
end if

;************************************************
;   Total precipitation code4  CMOR: pr
;************************************************
    Cvar="pr"
    fili    = workdir+"/"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc" 
 print("fili "+fili)
    mainTitle = "Total precipitation [mm/d]"
    subTitle  = run+" "+string_time


if (isfilepresent(fili)) then

;***read code pr
    File      = addfile (fili , "r")  
    var     = File->$Cvar$(0,:)       ;(time|0,cell|:)   
    var = var * 86400   
;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot

    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("precip_11lev") 

    missColor = (/0.5,0.5,0.5,0.5/)       ; add gray to color map
         
;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = cmap
    res@cnLevelSelectionMode = "ExplicitLevels"   ; set explicit contour levels
    res@cnLevels             = (/2,4,6,8,10,15,20,25,30,35,40/)
    res@cnFillColors         = (/0,1,2,3,4,5,6,7,8,9,10,11/)
    res@cnMissingValFillColor = missColor    ; gray 


    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0. )

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
    delete(res@cnFillPalette)
    delete(cmap)
    delete(missColor)
end if

;************************************************
;   Column water vapour CMOR: prw 
;************************************************

;***setings
    Cvar="prw"
    fili    = workdir+"/"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc"  
 print("fili "+fili)

    mainTitle = "Column water vapour [kg/m~S~2~N~]"
    subTitle  = run+" "+string_time



if (isfilepresent(fili)) then

    File       = addfile (fili , "r")   
    var     = File->$Cvar$(0,:)       ;(time|0,cell|:)  

;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("amwg_blueyellowred")    
    colors = new((/21,4/),float)                  ;-- assign color array
    colors(0:15,:)  = cmap(0:15,:)          
    colors(16,:) = (/1.,1.,1.,1./)                ; add white
    colors(17,:) = (/.8,.8,.8,1./)                ; add gray to color map
    colors(18,:) = (/.6,.6,.6,1./)
    colors(19,:) = (/.4,.4,.4,1./)
    colors(20,:) = (/.4,.0,.4,1./)
    missColor =  (/0.5,0.5,0.5,0.5/)     ; add gray to color map

;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = colors
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/10,20,30,40,50,60,70/)
    res@cnFillColors         = (/16,17,18,19,7,4,1,15/)
    res@cnMissingValFillColor = missColor    ; gray 



    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0. )

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
    delete(res@cnFillPalette)
    delete(cmap)
    delete(colors)
    delete(missColor)
end if

;************************************************
;   Surface Pressure code151  CMOR: psl
;************************************************

    Cvar="psl"

    fili    = workdir+"/"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc" 
 print("fili "+fili)
    mainTitle = "Surface Pressure [hPa]"
    subTitle  = run+" "+string_time



if (isfilepresent(fili)) then

;***read code 151
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,:)       ;(time|0,ncells|:)  
var=var*0.01   
print(Cvar+" min  "+min(var)+"  max  "+max(var))
 
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 
;-- define a new color map
    cmap =  read_colormap_file("nrl_sirkes_nowhite")
    colors = new((/25,4/),float)                  ;-- assign color array
    colors(0:18,:)  = cmap(0:18,:)          
    colors(19,:) = (/1.,1.,1.,1./)                ; add white
    colors(20,:) = (/.8,.8,.8,1./)                ; add gray to color map
    colors(21,:) = (/.6,.6,.6,1./)
    colors(22,:) = (/.4,.4,.4,1./)
    colors(23,:) = (/.2,.2,.2,1./)
    colors(24,:) = (/.9,.9,.9,1./)                ; add grey
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 

;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = colors
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/700,800,900,950,993,997,1001,1005,1009,1013,1017,1021,1025,1029,1033/)
    res@cnFillColors         = (/20,21,22,23,0,2,3,4,6,8,9,10,12,14,16,18/)
    res@lbLabelAngleF= 45	; tilt the XB labels 45 degrees
    res@lbLabelFontHeightF   =  0.008            ; label font height
    res@cnMissingValFillColor = missColor


    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0. )

    delete (res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
    delete(res@cnFillPalette)
    delete(cmap)
    delete(colors)
    delete(missColor)
end if


;************************************************
;   2 m Temperature code167  Cmor: tas
;************************************************

    Cvar="tas"
    fili    = workdir+"/"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc" 
 print("fili "+fili)
    mainTitle = "2 m Temperature [C]"
    subTitle  = run+" "+string_time




if (isfilepresent(fili)) then

;***read code 167
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,0,:)         ; dims: (time, height, cell)
    var = var -273.15
;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("t2m_29lev")
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = cmap
    res@cnLevelSelectionMode = "ExplicitLevels"   ; set explicit contour levels
    res@cnLevels             = (/-30,-25,-20,-15,-10,-5,0,5,10,15,20,25,30/)
    res@cnFillColors         = (/0,3,5,6,8,9,10,12,15,19,21,23,25,29/)
    res@cnMissingValFillColor = missColor

    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0. )

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(res@cnFillPalette)
    delete(var)
    delete(cmap)
    delete(missColor)
end if


;************************************************
;   Surface temperature code169 CMOR: ts
;************************************************

;***setings
    Cvar="ts" 
    fili    = workdir+"/"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc"  
 print("fili "+fili)
    mainTitle = "Surface temperature [C]"
    subTitle  = run+" "+string_time


                
if (isfilepresent(fili)) then
;***read 
    File    = addfile (fili , "r")  
    var     = File->$Cvar$(0,:)       ;(time|0,cell|:)   
    var = var -273.15
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("t2m_29lev")
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


             
;***create plot
    res                       = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = cmap
    res@cnLevelSelectionMode  = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels              = (/-30,-25,-20,-15,-10,-5,0,5,10,15,20,25,30/)
    res@cnFillColors         = (/0,3,5,6,8,9,10,12,15,19,21,23,25,29/) 

    res@cnMissingValFillColor = missColor

    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0. )


    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(res@cnFillPalette)
    delete(var)
    delete(cmap)
    delete(missColor)
end if


;************************************************
;   Liquid water + ice content code231_150 
;************************************************
;***setings
    Cvar="clwvi"
    fili     = workdir+"/"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc" 
    
    mainTitle = "Liq water + Ice cont [kg/m~S~2~N~]"
    subTitle  = run+" "+string_time

                
if (isfilepresent(fili)) then
;***read 
    File    = addfile (fili , "r")  
    var     = File->$Cvar$(0,:)       ;(time|0,ncells|:)   ; dims: (time,ncells)    
;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

    cmap =  read_colormap_file("amwg_blueyellowred")
    colors = new((/22,4/),float)                  ;-- assign color array
    colors(0:15,:)  = cmap(0:15,:)          
    colors(16,:) = (/1.,1.,1.,1./)                ; add white
    colors(17,:) = (/.8,.8,.8,1./)                ; add gray to color map
    colors(18,:) = (/.6,.6,.6,1./)
    colors(19,:) = (/.4,.4,.4,1./)
    colors(20,:) = (/.2,.2,.2,1./)
    colors(21,:) = (/.9,.9,.9,1./)                ; add grey
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map              
;***create plot
    res                      = True             ; plot mods desired
    res@cnFillPalette = colors
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/0.005,0.01,0.03,0.05,0.07,0.1,0.15,0.2,0.3,0.4/)
    res@cnFillColors         = (/16,21,17,18,19,20,7,6,4,3,1/)
    res@cnMissingValFillColor = missColor

    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0. )

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
end if

;************************************************
;   Liquid water 
;************************************************
;***setings
    Cvar="cllvi"
    fili     = workdir+"/"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc" 
    
    mainTitle = "Liquid water  [kg/m~S~2~N~]"
    subTitle  = run+" "+string_time

                
if (isfilepresent(fili)) then
;***read 
    File    = addfile (fili , "r")  
    var     = File->$Cvar$(0,:)       ;(time|0,ncells|:)   ; dims: (time,ncells)    
;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 


    cmap =  read_colormap_file("amwg_blueyellowred")
    colors = new((/22,4/),float)                  ;-- assign color array
    colors(0:15,:)  = cmap(0:15,:)          
    colors(16,:) = (/1.,1.,1.,1./)                ; add white
    colors(17,:) = (/.8,.8,.8,1./)                ; add gray to color map
    colors(18,:) = (/.6,.6,.6,1./)
    colors(19,:) = (/.4,.4,.4,1./)
    colors(20,:) = (/.2,.2,.2,1./)
    colors(21,:) = (/.9,.9,.9,1./)                ; add grey
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map              
;***create plot
    res                      = True             ; plot mods desired
    res@cnFillPalette = colors
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/0.005,0.01,0.03,0.05,0.07,0.1,0.15,0.2,0.3,0.4/)
    res@cnFillColors         = (/16,21,17,18,19,20,7,6,4,3,1/)

    res@cnMissingValFillColor = missColor

    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0. )

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
end if

;************************************************
;   Ice cont
;************************************************
;***setings
    Cvar="clivi"
    fili     =  workdir+"/"+run+"_"+meantime+"_"+yyyy+"-"+yyyy+"_atm_2d.nc" 
    
    mainTitle = "Ice content  [kg/m~S~2~N~]"
    subTitle  = run+" "+string_time

                
if (isfilepresent(fili)) then
;***read 
    File    = addfile (fili , "r")  
    var     = File->$Cvar$(0,:)       ;(time|0,ncells|:)   ; dims: (time,ncells)    
;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 


    cmap =  read_colormap_file("amwg_blueyellowred")
    colors = new((/22,4/),float)                  ;-- assign color array
    colors(0:15,:)  = cmap(0:15,:)          
    colors(16,:) = (/1.,1.,1.,1./)                ; add white
    colors(17,:) = (/.8,.8,.8,1./)                ; add gray to color map
    colors(18,:) = (/.6,.6,.6,1./)
    colors(19,:) = (/.4,.4,.4,1./)
    colors(20,:) = (/.2,.2,.2,1./)
    colors(21,:) = (/.9,.9,.9,1./)                ; add grey
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map              
;***create plot
    res                      = True             ; plot mods desired
    res@cnFillPalette = colors
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/0.005,0.01,0.03,0.05,0.07,0.1,0.15,0.2,0.3,0.4/)
    res@cnFillColors         = (/16,21,17,18,19,20,7,6,4,3,1/)

    res@cnMissingValFillColor = missColor

    fast_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle,comment,0. )

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
end if

end
