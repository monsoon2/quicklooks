load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl" 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl" 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"

undef("reg_cell")
 procedure reg_cell  (wks,var,x,y,cArea,vlon,vlat,cnres_in,missColor,pltName,\
                        mainTitle,subTitle)
;************************************************
;  
;************************************************
local plot, cnres, mpres, pres, res

begin

  nv   = dimsizes(vlon(0,:))                      ; no of points in polygon
;***   latMin            =  20.      ; Europe
;***   latMax            =  75.
;***   lonMin            = -20.
;***   lonMax            =  40.
;***   latMin            = -10      ; Tropical Atlantic
;***   latMax            =  20.
;***   lonMin            = -80.
;***   lonMax            =  40.

   latMin            = -10      ; Tropical Pacific
   latMax            =  30.
   lonMin            =  80.
   lonMax            = 170.


  print("")
  print("Data longitude min/max: " + min(vlon) + "   " + max(vlon))
  print("")
  print("Plot area: "+lonMin+","+lonMax+" "+latMin+","+latMax)
  print("")

;***set contour resources
  res                      =  True
  res@gsnDraw              =  True        ;-- don't draw the plot
  res@gsnFrame             =  False        ;-- don't advance the frame
  res@cnLinesOn            =  False        ;-- don't draw contour lines
  res@cnInfoLabelOn        =  False        ;-- switch off contour info label
  res@cnFillOn             =  True         ;-- contour fill on

  res@cnFillMode           = "CellFill"    ;-- set fill mode
  res@sfXArray             =  x            ;-- transform x to mesh scalar field
  res@sfYArray             =  y            ;-- transform y to mesh scalar field
  res@sfXCellBounds        =  vlon         ;-- needed if cnFillMode= "CellFill"
  res@sfYCellBounds        =  vlat         ;-- needed if cnFillMod = "CellFill"
  
  res@lbLabelBarOn         =  True      ;-- don't draw a labelbar yet

  res@mpFillOn             =  True                    ;?-- fill map grey
  res@mpOutlineOn          =  True 
;  res@mpLandFillColor      =  18
  res@mpFillColors            =(/"transparent","transparent","transparent","transparent"/)
  res@mpMinLonF           =  lonMin         ;-- sub-region minimum longitude
  res@mpMaxLonF           =  lonMax         ;-- sub-region maximum longitude
  res@mpMinLatF           =  latMin         ;-- sub-region minimum latitude
  res@mpMaxLatF           =  latMax         ;-- sub-region maximum latitude

  res@mpGreatCircleLinesOn =  False          ;-- important: v6.2.0 False !!
  res@gsnSpreadColors      =  False  
  res@gsnCenterString      = " "
  res@gsnLeftString        = subTitle 
  res@gsnRightString       = mainTitle 
  res@txFontHeightF   = 0.014   

    res@gsnSpreadColors      = False     ; use full range of colors
res@lbAutoManage = False
    res@pmLabelBarOrthogonalPosF = 0.2          ; move farther to plot
    res@lbOrientation        = "horizontal"      ; vertical label bar
    res@lbLabelFontHeightF   =  0.012            ; label font height

  res=cnres_in 


;***create the contour plot, but don't draw it. 
;***We need it to get the colors of the data values

  plot = gsn_csm_contour_map(wks,var,res)

  meanV=dim_avg(var*cArea)/dim_avg(cArea)
  minV=min(var)
  maxV=max(var)
  print("min: "+minV+ "  mean: "+ meanV+ "  max: "+ maxV)

  statV="min="+sprintf("%8.3f", minV)+"  mean="+sprintf("%8.3f", meanV)+"  max="+sprintf("%8.3f", maxV)
  txres               = True                    ; text mods desired
  txres@txFontHeightF = 0.011 
print(statV)
;  gsn_text_ndc(wks,statV,0.5,0.19,txres)
  gsn_text_ndc(wks,statV,0.5,0.25,txres)

  frame(wks)                                ; now plot the frame

end 
;***end procedure reg_cell 
;************************************************
; main
;************************************************
begin




  values    = asciiread("var.txt",4,"string")
  run = values(0)
  meantime = values(1)
  yyyy = values(2)
  workdir = values(3)        ; plot & input directory


print("run: "+run)
print("meantime: "+meantime)
string_time=meantime+" "+yyyy
print("string_time: "+string_time)
suffix="_TP"

;***define plot
   pltType = "png"                        ; x11, eps, ps, pdf, png

  pltPath = workdir+"/"+run+"_"+meantime+"_"+yyyy+"_"  

;************************************************
;  Read grid information
;  define the x-, y-values and the polygon points
;************************************************
;  GridInfoFileName = systemfunc("echo $GrdInfoFile")
GridInfoFileName ="/work/mh0081/m214091/ICON-luis/"+run+"/tas/EU_tas_1d-40d.nc"
GridInfoFileName =workdir+"/TP_"+run+"_atm_2d_area.nc" 
GridInfoFileName ="TP_"+run+"_area.nc" 
print(GridInfoFileName)

  GridInfoFile= addfile( GridInfoFileName, "r" )

  rad2deg = 45./atan(1.)            ; radians to degrees

  x = GridInfoFile->clon *rad2deg   ; cell center, lon
  y = GridInfoFile->clat *rad2deg   ; cell center, lat

  cArea = GridInfoFile->cell_area   ; area of grid cell

; note: clon and clat are longitude and latitude of triangle centers.
;       Locations of the cell corners are given by 
;       clon_vertices and clat_vertices in the nc file.


  x!0     = "lon"
  x@units = "degrees_east"
  y!0     = "lat"
  y@units = "degrees_north"
  vlon    = GridInfoFile->clon_bnds * rad2deg
  vlat    = GridInfoFile->clat_bnds * rad2deg
;printVarSummary(vlon)
;printVarSummary(cArea)
;printVarSummary(x)
;printVarSummary(y)

;** use by differences
;**   fldstd = tofloat (systemfunc("cdo output -fldstd -setgrid,$GrdInfoFile "+fili))

;************************************************
;   olr
;************************************************

;***setings
    Cvar="olr"
    fili    = workdir+"/TP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc"
 print("fili "+fili)  
    mainTitle = "olr [W/m~S~2~N~]"
    subTitle  = run+" "+string_time

if (isfilepresent(fili)) then

;***read code 
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,:)         ; dims: (time, height, cell)
    var = (-1) *var
    print("ploted: "+mainTitle)

;***open plot sfcWind
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("MPL_Greys")
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = cmap
    res@cnSpanFillPalette   =  True  ;instead of gsnSpreadColors, used by cnFillPalette
    res@mpGeophysicalLineColor = "orange"
    res@cnLevelSelectionMode = "ManualLevels"
    res@cnMinLevelValF = 200
    res@cnMaxLevelValF = 320
    res@cnLevelSpacingF = 10

    res@cnMissingValFillColor = missColor

    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle)

delete(res@mpGeophysicalLineColor) 
delete(res@cnSpanFillPalette)
    delete(res@cnFillPalette)
    delete(var)
    delete(cmap)
    delete(missColor)
end if

;************************************************
;   Wind speed
;************************************************

;***setings
    Cvar="sfcwind"
    fili    = workdir+"/TP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc"
 print("fili "+fili)  
    mainTitle = "Wind Speed [m/s]"
    subTitle  = run+" "+string_time

if (isfilepresent(fili)) then

;***read code 
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,0,:)         ; dims: (time, height, cell)
    print("ploted: "+mainTitle)

;***open plot sfcWind
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("wind_17lev")
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = cmap
    res@cnSpanFillPalette   =  True  ;instead of gsnSpreadColors, used by cnFillPalette
    res@cnLevelSelectionMode = "ManualLevels"
    res@cnMinLevelValF = 2
    res@cnMaxLevelValF = 20
    res@cnLevelSpacingF = 2

    res@cnMissingValFillColor = missColor

    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle)

    delete(res@cnSpanFillPalette)
    delete(res@cnFillPalette)
    delete(var)
    delete(cmap)
    delete(missColor)
end if

;************************************************
;   latent heat flux 
;************************************************

;***setings
    Cvar="hfls"
    fili    =  workdir+"/TP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc"
     print("fili "+fili)  
    mainTitle = "latent heat flux [W/m~S~2~N~,up]"
    subTitle  = run+" "+string_time

if (isfilepresent(fili)) then

;***read code 
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,:)         ; dims: (time, height, cell)
    var     = var * (-1)
    print("ploted: "+mainTitle)

;***open plot sfcWind
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("BlueDarkRed18")
    colors = new((/20,4/),float)                  ;-- assign color array
    colors(0:17,:)  = cmap(0:17,:)          
    colors(18,:) = (/1.,1.,1.,1./)                ; add white
    colors(19,:) = (/.9,0.,.9,1./)                ; add pink
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = colors

    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/-60,-30,30,60,90,120,150,180,210,240/)
;    res@cnFillColors         = (/5,6,7,8,9,17,11,12,13,14,15,16/)
    res@cnFillColors         = (/1,7,18,10,11,12,13,15,16,17,19/)
    res@cnMissingValFillColor = missColor  

    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle)

    delete(colors)
    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(res@cnFillPalette)
    delete(var)
    delete(cmap)
    delete(missColor)
end if

;************************************************
;   sensible heat flux 
;************************************************

;***setings
    Cvar="hfss"
    fili    = workdir+"/TP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc" 
    print("fili "+fili)  
    mainTitle = "sensible heat flux [W/m~S~2~N~,up]"
    subTitle  = run+" "+string_time

if (isfilepresent(fili)) then

;***read code 
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,:)         ; dims: (time, height, cell)
    var     = var * (-1)
    print("ploted: "+mainTitle)

;***open plot sfcWind
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("BlueDarkRed18")
    colors = new((/20,4/),float)                  ;-- assign color array
    colors(0:17,:)  = cmap(0:17,:)          
    colors(18,:) = (/1.,1.,1.,1./)                ; add white
    colors(19,:) = (/.9,0.,.9,1./)                ; add pink
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = colors

    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/-100,-80,-60,-40,-20,-10,10,20,40,60,80,100,120/)
;    res@cnFillColors         = (/5,6,7,8,9,10,17,11,12,13,14,15,16/)
    res@cnFillColors         = (/1,3,4,5,7,8,18,10,12,13,14,16,17,19/)
    res@cnMissingValFillColor = missColor  

    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle)

    delete(colors)
    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(res@cnFillPalette)
    delete(var)
    delete(cmap)
    delete(missColor)
end if

;************************************************
;   Zonal wind stress CMOR: tauu only sea
;************************************************

;***setings
    Cvar="tauu"
    fili    = workdir+"/XTP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc"    
    mainTitle = "Zonal wind stress [mN/m~S~2~N~]"
    subTitle  = run+" "+string_time



if (isfilepresent(fili)) then

;***read code tauu
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,:)        ; dims: (time,ncells)
    var = var * 1000
    print("ploted: "+mainTitle)
printVarSummary(var)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("amwg_blueyellowred")
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = cmap
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/-200,-100,-50,-20,0,20,50,100,200/)
    res@cnFillColors         = (/0,2,4,6,7,8,9,10,12,13,14/)
    res@cnMissingValFillColor = missColor

    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle)

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(res@cnFillPalette)
    delete(var)
    delete(cmap)
    delete(missColor)
end if



;************************************************
;   Total cloud cover CMOR: clt
;************************************************

;***setings
    Cvar="clt"
    fili    = workdir+"/TP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc" 
       
    mainTitle = "Total cloud cover [%]"
    subTitle  = run+" "+string_time


                
if (isfilepresent(fili)) then
;***read code clt
    File    = addfile (fili , "r")  
    var     = File->$Cvar$(0,:)       ;(time|0,ncells|:)   ; dims: (time,ncells)    
    var     = var * 100
;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix          ; plot name
    wks     = gsn_open_wks(pltType, pltName) 

    cmap =  read_colormap_file("amwg_blueyellowred")
    colors = new((/22,4/),float)                  ;-- assign color array
    colors(0:15,:)  = cmap(0:15,:)          
    colors(16,:) = (/1.,1.,1.,1./)                ; add white
    colors(17,:) = (/.8,.8,.8,1./)                ; add gray to color map
    colors(18,:) = (/.6,.6,.6,1./)
    colors(19,:) = (/.4,.4,.4,1./)
    colors(20,:) = (/.2,.2,.2,1./)
    colors(21,:) = (/.9,.9,.9,1./)                ; add grey

    missColor = (/0.5,0.5,0.5,0.5/) 
;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = colors
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/10,20,30,40,50,60,70,80,90/)
    res@cnFillColors         = (/1,3,4,6,7,21,17,18,19,20/)
    res@cnMissingValFillColor =  missColor    ; gray 

    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle)

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
    delete(res@cnFillPalette)
    delete(cmap)
    delete(missColor)
    delete(colors)
end if

;************************************************
;   Total precipitation code4  CMOR: pr
;************************************************
    Cvar="pr"
    fili    = workdir+"/TP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc"
 print("fili: "+fili)
    mainTitle = "Total precipitation [mm/d]"
    subTitle  = run+" "+string_time


if (isfilepresent(fili)) then

;***read code pr
    File      = addfile (fili , "r")  
    var     = File->$Cvar$(0,:)       ;(time|0,ncells|:)     
    var     = var * 86400 
;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot

    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("precip_11lev") 

    missColor = (/0.5,0.5,0.5,0.5/)       ; add gray to color map
         
;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = cmap
    res@cnLevelSelectionMode = "ExplicitLevels"   ; set explicit contour levels
    res@cnLevels             = (/2,4,6,8,10,15,20,25,30,35,40/)
    res@cnFillColors         = (/0,1,2,3,4,5,6,7,8,9,10,11/)
    res@cnMissingValFillColor = missColor    ; gray 


    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle)

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
    delete(res@cnFillPalette)
    delete(cmap)
    delete(missColor)
end if

;************************************************
;   Column water vapour CMOR: prw 
;************************************************

;***setings
    Cvar="prw"
    fili    = workdir+"/TP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc"

    mainTitle = "Column water vapour [kg/m~S~2~N~]"
    subTitle  = run+" "+meantime



if (isfilepresent(fili)) then

    File       = addfile (fili , "r")   
    var     = File->$Cvar$(0,:)       ;(time|0,ncells|:)  
;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("amwg_blueyellowred")
 ;   colors = new((/21,4/),float)                  ;-- assign color array
 ;    colors(0:15,:)  = cmap(0:15,:)          
  ;   colors(16,:) = (/1.,1.,1.,1./)                ; add white
  ;  colors(17,:) = (/.8,.8,.8,1./)                ; add gray to color map
  ;   colors(18,:) = (/.6,.6,.6,1./)
  ;   colors(19,:) = (/.4,.4,.4,1./)
  ;   colors(20,:) = (/.4,.0,.4,1./)
   colors = new((/22,4/),float)                  ;-- assign color array
    colors(0:15,:)  = cmap(0:15,:)          
    colors(16,:) = (/1.,1.,1.,1./)                ; add white
    colors(17,:) = (/.8,.8,.8,1./)                ; add gray to color map
    colors(18,:) = (/.6,.6,.6,1./)
    colors(19,:) = (/.4,.4,.4,1./)
    colors(20,:) = (/.2,.2,.2,1./)
    colors(21,:) = (/.9,.9,.9,1./)      
    missColor =  (/0.5,0.5,0.5,0.5/)     ; add gray to color map

;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = colors
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/4,8,12,16,20,24,28,32,36/)
    res@cnFillColors         = (/16,17,18,19,20,7,4,1,15,0/)
    res@cnMissingValFillColor = missColor    ; gray 



    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle)

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
    delete(res@cnFillPalette)
    delete(cmap)
    delete(colors)
    delete(missColor)
end if

;************************************************
;   Surface Pressure code151  CMOR: psl
;************************************************

    Cvar="psl"
    fili    = workdir+"/TP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc"
print(fili)
    mainTitle = "Surface Pressure [hPa]"
    subTitle  = run+" "+string_time


if (isfilepresent(fili)) then

;***read code 151
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,:)       ;(time|0,ncells|:)  
var=var*0.01   

    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 
;-- define a new color map
    cmap =  read_colormap_file("nrl_sirkes_nowhite")
    colors = new((/25,4/),float)                  ;-- assign color array
    colors(0:18,:)  = cmap(0:18,:)          
    colors(19,:) = (/1.,1.,1.,1./)                ; add white
    colors(20,:) = (/.8,.8,.8,1./)                ; add gray to color map
    colors(21,:) = (/.6,.6,.6,1./)
    colors(22,:) = (/.4,.4,.4,1./)
    colors(23,:) = (/.2,.2,.2,1./)
    colors(24,:) = (/.9,.9,.9,1./)                ; add grey
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 

;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = colors
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/700,800,900,950,993,997,1001,1005,1009,1013,1017,1021,1025,1029,1033/)
    res@cnFillColors         = (/20,21,22,23,0,2,3,4,6,8,9,10,12,14,16,18/)
    res@lbLabelAngleF= 45	; tilt the XB labels 45 degrees
    res@lbLabelFontHeightF   =  0.008            ; label font height
    res@cnMissingValFillColor = missColor


    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle)

    delete (res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
    delete(res@cnFillPalette)
    delete(cmap)
    delete(colors)
    delete(missColor)
end if


;************************************************
;   2 m Temperature code167  Cmor: tas
;************************************************

    Cvar="tas"
    fili    = workdir+"/TP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc"
print(fili)
    mainTitle = "2 m Temperature [C]"
    subTitle  = run+" "+string_time




if (isfilepresent(fili)) then

;***read code 167
    File       = addfile (fili , "r") 
    var     = File->$Cvar$(0,0,:)         ; dims: (time, height, cell)
    var = var -273.15
;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("t2m_29lev")
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


;***create plot
    res                      = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = cmap
    res@cnLevelSelectionMode = "ExplicitLevels"   ; set explicit contour levels
    res@cnLevels             = (/0,3,6,9,12,15,18,21,24,27,30,33,36/)
    res@cnFillColors         = (/10,11,13,15,16,18,19,20,21,23,25,27,28,29/)
    res@cnMissingValFillColor = missColor

    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle)

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(res@cnFillPalette)
    delete(var)
    delete(cmap)
    delete(missColor)
end if


;************************************************
;   Surface temperature code169 CMOR: ts
;************************************************

;***setings
    Cvar="ts"  
    fili    = workdir+"/TP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc"
print(fili)
    mainTitle = "Surface temperature [C]"
    subTitle  = run+meantime
    subTitle  = run+" "+string_time

                
if (isfilepresent(fili)) then
;***read 
    File    = addfile (fili , "r")  
    var     = File->$Cvar$(0,:)       ;(time|0,ncells|:)   ; dims: (time,ncells)    
    var = var -273.15
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

;-- define a new color map
    cmap =  read_colormap_file("t2m_29lev")
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map 


             
;***create plot
    res                       = True             ; plot mods desired
    res@mpFillOn  =  False
    res@cnFillPalette = cmap
    res@cnLevelSelectionMode  = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/0,3,6,9,12,15,18,21,24,27,30,33,36/)
    res@cnFillColors         = (/10,11,13,15,16,18,19,20,21,23,25,27,28,29/)
;    res@cnLevels              = (/-30,-25,-20,-15,-10,-5,0,5,10,15,20,25,30/)
;    res@cnFillColors         = (/0,3,5,6,8,9,10,12,15,19,21,23,25,29/) 
    res@cnMissingValFillColor = missColor

    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle)

    delete(res@cnFillPalette)
    delete(cmap)
    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
end if

;************************************************
;   Liquid water + ice content code231_150 
;************************************************
;***setings
    Cvar="clwvi"
    fili    = workdir+"/TP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc" 
   
    mainTitle = "Liq water + Ice cont [kg/m~S~2~N~]"
    subTitle  = run+" "+string_time

                
if (isfilepresent(fili)) then
;***read 
    File    = addfile (fili , "r")  
    var     = File->$Cvar$(0,:)       ;(time|0,ncells|:)   ; dims: (time,ncells)    
;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 

    cmap =  read_colormap_file("amwg_blueyellowred")
    colors = new((/22,4/),float)                  ;-- assign color array
    colors(0:15,:)  = cmap(0:15,:)          
    colors(16,:) = (/1.,1.,1.,1./)                ; add white
    colors(17,:) = (/.8,.8,.8,1./)                ; add gray to color map
    colors(18,:) = (/.6,.6,.6,1./)
    colors(19,:) = (/.4,.4,.4,1./)
    colors(20,:) = (/.2,.2,.2,1./)
    colors(21,:) = (/.9,.9,.9,1./)                ; add grey
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map              
;***create plot
    res                      = True             ; plot mods desired
    res@cnFillPalette = colors
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/0.005,0.01,0.03,0.05,0.07,0.1,0.15,0.2,0.3,0.4/)
    res@cnFillColors         = (/16,21,17,18,19,20,7,6,4,3,1/)
    res@cnMissingValFillColor = missColor

    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle )

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
end if

;************************************************
;   Liquid water 
;************************************************
;***setings
    Cvar="cllvi"
    fili    = workdir+"/TP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc"    

    mainTitle = "Liquid water  [kg/m~S~2~N~]"
    subTitle  = run+" "+string_time

                
if (isfilepresent(fili)) then
;***read 
    File    = addfile (fili , "r")  
    var     = File->$Cvar$(0,:)       ;(time|0,ncells|:)   ; dims: (time,ncells)    
;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 


    cmap =  read_colormap_file("amwg_blueyellowred")
    colors = new((/22,4/),float)                  ;-- assign color array
    colors(0:15,:)  = cmap(0:15,:)          
    colors(16,:) = (/1.,1.,1.,1./)                ; add white
    colors(17,:) = (/.8,.8,.8,1./)                ; add gray to color map
    colors(18,:) = (/.6,.6,.6,1./)
    colors(19,:) = (/.4,.4,.4,1./)
    colors(20,:) = (/.2,.2,.2,1./)
    colors(21,:) = (/.9,.9,.9,1./)                ; add grey
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map              
;***create plot
    res                      = True             ; plot mods desired
    res@cnFillPalette = colors
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/0.005,0.01,0.03,0.05,0.07,0.1,0.15,0.2,0.3,0.4/)
    res@cnFillColors         = (/16,21,17,18,19,20,7,6,4,3,1/)

    res@cnMissingValFillColor = missColor

    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle)

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
end if

;************************************************
;   Ice cont
;************************************************
;***setings
    Cvar="clivi"
    fili    = workdir+"/TP_atm_2d.nc"  ;workdir+"/"+run+"/"+Cvar+"/TP_"+Cvar+"_"+meantime+".nc"    

    mainTitle = "Ice content  [kg/m~S~2~N~]"
    subTitle  = run+" "+string_time

                
if (isfilepresent(fili)) then
;***read 
    File    = addfile (fili , "r")  
    var     = File->$Cvar$(0,:)       ;(time|0,ncells|:)   ; dims: (time,ncells)    
;   printVarSummary(var)
    print("ploted: "+mainTitle)

;***open plot
    pltName = pltPath+Cvar+suffix
    wks     = gsn_open_wks(pltType, pltName) 


    cmap =  read_colormap_file("amwg_blueyellowred")
    colors = new((/22,4/),float)                  ;-- assign color array
    colors(0:15,:)  = cmap(0:15,:)          
    colors(16,:) = (/1.,1.,1.,1./)                ; add white
    colors(17,:) = (/.8,.8,.8,1./)                ; add gray to color map
    colors(18,:) = (/.6,.6,.6,1./)
    colors(19,:) = (/.4,.4,.4,1./)
    colors(20,:) = (/.2,.2,.2,1./)
    colors(21,:) = (/.9,.9,.9,1./)                ; add grey
    missColor = (/0.5,0.5,0.5,0.5/)      ; add gray to color map              
;***create plot
    res                      = True             ; plot mods desired
    res@cnFillPalette = colors
    res@cnLevelSelectionMode = "ExplicitLevels" ; set explicit contour levels
    res@cnLevels             = (/0.005,0.01,0.03,0.05,0.07,0.1,0.15,0.2,0.3,0.4/)
    res@cnFillColors         = (/16,21,17,18,19,20,7,6,4,3,1/)

    res@cnMissingValFillColor = missColor

    reg_cell ( wks,var,x,y,cArea,vlon,vlat,res,missColor,pltName,\
                mainTitle,subTitle)

    delete(res@cnLevels)
    delete (res@cnFillColors)
    delete(var)
end if

end
